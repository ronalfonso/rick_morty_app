import express from 'express';
import http from 'http';
import dotenv from 'dotenv';
import cors from 'cors';

//Routes
import userRoutes from '../routes/users.js'
import characterRoutes from '../routes/character.js'

const app = express();

dotenv.config({path: './config/config.env'});

import {Error} from '../middleware/error.js';

const error = new Error();

//Set Port
const PORT = process.env.PORT || 2112;

export default class Server {
    constructor() {
        this.iniRoutes();
        this.start();
    }

    iniRoutes() {
        app.use(express.json());
        app.use(cors());

        app.use('/api/v1/users', userRoutes);
        app.use('/api/v1/characters', characterRoutes);
    }

    start() {
        app.use(error.errorHandler);

        const server = http.createServer(app);
        server.listen(PORT, () => {
            console.log(`Server runing on port: ${PORT}`);
        });

    }

}

new Server();

