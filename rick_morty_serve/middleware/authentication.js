
import jwt from 'jsonwebtoken'

export class VerifyToken {
    constructor() {
    }

    verifyToken(req, res, next) {
        let token = req.get('token');

        jwt.verify(token, process.env.TOKEN_HASH, (err, decoded) => {

            if (err) {
                return res.status(401).json({
                    ok: false,
                    err
                })
            }
            next();
        })
    }

}

