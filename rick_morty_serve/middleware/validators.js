import bcrypt from 'bcryptjs';
import redis from 'redis';
import {ErrorResponse} from '../utils/errorResponse.js';

//Set Port
const REDIS_PORT = process.env.PORT || 6379;
const client = redis.createClient(REDIS_PORT, process.env.HOST);
const salt = process.env.PASSWORD_SALT;

export class Validators {
    constructor() {
    }

    passHash(password) {
        return bcrypt.hash(password, parseInt(salt));
    }

    userExists(username, message, next) {
        return client.exists(username, (err, exist) => {
            if (err) return console.log(err)
            if (exist === 1) {
                return next(new ErrorResponse(message, 404));
            }
        })
    }

    userNoExists(username, message, next) {
        return client.exists(username, (err, exist) => {
            if (err) return console.log(err)
            if (exist === 0) {
                return next(new ErrorResponse(message, 404));
            }
        })
    }

    missingsParams(body, message, next) {
        const {name, lastName, username, password} = body;
        if (!username || !password || !lastName || !name) {
            console.log('falta parametro');
            return next(new ErrorResponse('Invalid params', 400));
        }
    }
}
