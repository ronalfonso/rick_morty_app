import {ErrorResponse} from '../utils/errorResponse.js'

const errorDictionary = [
    {
        errorIdentifiers: ['23505'],
        getErrorResponse: (err) => new ErrorResponse(`${err.detail}`, 400),
    },
];

export class Error {
    constructor() {
    }

    getErrorResponseFromDictionary(err) {
        const errorCriteria = err.code || err.name;
        const error = errorDictionary.find((errDic) => {
                return errDic.errorIdentifiers.includes(errorCriteria)
            }

        );

        if (!error) return new ErrorResponse('Internal Server Error', 500);

        return error.getErrorResponse(err);
    }

    errorHandler(err, req, res, next) {
        //Log to console for the development
        const customError = err instanceof ErrorResponse ? err : this.getErrorResponseFromDictionary(err);
        console.log(err)
        res
            .status(customError.statusCode || 500)
            .json({ success: false, error: customError.message || 'Server Error' });
    }

}

// const getErrorResponseFromDictionary = (err) => {
//
// };
//
// const errorHandler = (err, req, res, next) => {
//
// };
//
// module.exports = errorHandler;
