import {UsersController} from './../controllers/users.js';
import axios from 'axios';
import {mockNext, mockRequest, mockResponse} from './utils/interceptors.js';
import {ErrorResponse} from '../utils/errorResponse.js';
import redis from 'redis';


//Set Port
const REDIS_PORT = process.env.PORT || 6379;
const client = redis.createClient(REDIS_PORT, process.env.HOST);

jest.mock('axios');
const usersController = new UsersController();

describe("Test controller users", () => {

    const OLD_ENV = process.env;
    let req, res, next;

    beforeAll(() => {
        req = mockRequest();
        res = mockResponse();
        next = mockNext();
    });

    beforeEach(() => {
        jest.resetModules()
        process.env = {TOKEN_HASH: 'SECRET_HASH', TOKEN_EXPIRATION_SECONDS: 120000};
    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Should create user with api createUser', async () => {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user",
            "password": "1234"
        }
        axios.post.mockImplementationOnce(() => Promise.resolve(body));
        await expect(axios.post('http://localhost:5000/api/v1/users/', body)).resolves.toBeDefined();
    });

    it('Should login user with api login', async () => {
        const headers = {
            "username": "user",
            "password": "1234"
        }
        axios.get.mockImplementationOnce(() => Promise.resolve(headers));
        await expect(axios.get('http://localhost:5000/api/v1/users/', headers)).resolves.toBeDefined();
    });

    it('Should create user', async () => {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user",
            "password": "1234"
        }
        req.body = body;
        await usersController.createUser(req, res, next)
        expect(res.status).toHaveBeenCalledWith(200);
        client.del(body.username);

    });

    it('Should return error when user created', async () => {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user1",
            "password": "1234"
        }
        let keys = Object.keys(body);
        let randomProperty = keys[Math.floor(Math.random() * (keys.length))]
        delete body[randomProperty];

        req.body = body;
        await usersController.createUser(req, res, next)
        expect(next).toHaveBeenCalledWith(new ErrorResponse('Invalid params', 400))
    });

    it('should return error when username exits', async () => {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user",
            "password": "1234",
        }
        req.body = body;
        await usersController.createUser(req, res, next)
        expect(next).toHaveBeenCalledWith(new ErrorResponse('Username exist', 404))

    });

    it('Should user login with method in controller', async () => {
        const bodyUser = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user2",
            "password": "1234"
        }
        req.body = bodyUser;
        await usersController.createUser(req, res, next)

        const headersLogin = {
            "username": "user2",
            "password": "1234"
        }
        req.headers = headersLogin;
        await usersController.loginUser(req, res, next);
        expect(res.status.mock.calls.pop()[0]).toBe(200);
        client.del(bodyUser.username);
    });

    it('Should return error when get user without username', async () => {
        await usersController.getUserByUsername(req, res, next);
        expect(next).toHaveBeenCalledWith(new ErrorResponse('Please provide an username', 400))

    });

    it('Should get user by username', async () => {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user3",
            "password": "1234"
        }
        req.body = body;
        await usersController.createUser(req, res, next)

        req.params.username = 'user3';
        await usersController.getUserByUsername(req, res, next);
        expect(res.status.mock.calls.pop()[0]).toBe(200);
        client.del(body.username);
    });

    it('Should edit user by username', async () => {
        const bodyCreate = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user4",
            "password": "1234"
        }
        req.body = bodyCreate;
        await usersController.createUser(req, res, next)

        const bodyUpdate = {
            "name": "Alejandro",
            "lastName": "Parra",
            "username": "user4",
            "password": "12345"
        }
        req.body = bodyUpdate;
        await usersController.updateUserByUsername(req, res, next);
        expect(res.status).toHaveBeenCalledWith(200);
        client.del(bodyCreate.username);
    });

    it('Should return error when edit user without username', async () => {
        const bodyCreate = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user5",
            "password": "1234"
        }
        req.body = bodyCreate;
        await usersController.createUser(req, res, next)

        const bodyUpdate = {
            "name": "Alejandro",
            "lastName": "Parra",
            "password": "12345"
        }
        req.body = bodyUpdate;
        await usersController.updateUserByUsername(req, res, next);
        expect(next).toHaveBeenCalledWith(new ErrorResponse('Please provide an username', 400))
        client.del(bodyCreate.username);
    });


})

describe("Test logic redis", () => {
    it('should ', async () => {
        redis.setex("prueba", 3600, "prueba", (err, result) => {
            console.log(result);
        })

    });
})

