import {CharacterController} from "../controllers/character.js";
import {UsersController} from './../controllers/users.js';
import axios from 'axios';
import {mockNext, mockRequest, mockResponse} from './utils/interceptors.js';
import httpMocks from 'node-mocks-http';
import tokenGenerator from '../utils/tokenGenerator.js';

jest.mock('axios');
const characterController = new CharacterController();
const usersController = new UsersController();

describe("Test controller character", () => {

    const OLD_ENV = process.env;
    let req, res, next;

    beforeAll(() => {
        req = mockRequest();
        res = mockResponse();
        next = mockNext();
    });

    beforeEach(() => {
        jest.resetModules()
        process.env = {TOKEN_HASH: 'SECRET_HASH', TOKEN_EXPIRATION_SECONDS: 120000};
    });

    afterAll(() => {
        process.env = OLD_ENV;
    });

    it('Should get characters', async () => {
        const username = 'userCharacter'
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": username,
            "password": "1234"
        }
        req.body = body;
        await usersController.createUser(req, res, next);

        const headers = {
            ...tokenGenerator({username})
        }
        req.headers = headers;
        await characterController.getCharacter(req, res);
        expect(res.status).toHaveBeenCalledWith(200);

    });


})
