import {Helpers} from "./utils/helpers";
import should from "should";


const redisMock = new Helpers();

let r;

beforeEach(function () {
    r = redisMock.createClient();
});

afterEach(function (done) {
    r.flushall();
    r.quit(done);
});


describe("Test logic redis", () => {
    it('Should create user', async () => {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user",
            "password": "1234"
        }
        r.setex(body.username, 3600, JSON.stringify(body), (err, result) => {
            result.should.equal("OK")
        });
    });

    it('Should no exist username', function () {
        const body = {
            "name": "ronald",
            "lastName": "alfonso",
            "username": "user",
            "password": "1234"
        }
        r.setex("ronalfonso", 3600, JSON.stringify(body), (err, result) => {
            r.exists(body.username, (err, result) => {
                result.should.equal(0)
            })
        });
    });

})
