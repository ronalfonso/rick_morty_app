import redismock from 'redis-mock';
// import redis from 'redis';

// redismock = redis;

export class Helpers {
    constructor() {
    }

    createClient() {
        // var options = {
        //     detect_buffers: true,
        //     url: redismock
        // }
        return redismock.createClient(redismock);
    }
}
