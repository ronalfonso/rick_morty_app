import express from 'express';
const router = express.Router();

import { UsersController } from '../controllers/users.js';
import { VerifyToken }  from '../middleware/authentication.js';

const user = new UsersController();
const verify = new VerifyToken();

router.route('/').post(user.createUser).get(user.loginUser);

router.route('/:username').get(user.getUserByUsername);

router.route('/:username').put(verify.verifyToken, user.updateUserByUsername);

const userRoutes = router;
export default userRoutes;
