
import express from 'express';
const router = express.Router();

import { CharacterController } from '../controllers/character.js';
import { VerifyToken }  from '../middleware/authentication.js';

const character = new CharacterController();
const verify = new VerifyToken();

router.route('/:username').get(verify.verifyToken, character.getCharacter);

const characterRoutes = router;

export default characterRoutes;



