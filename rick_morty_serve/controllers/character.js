

import fetch from "node-fetch";

export class CharacterController {
    constructor() {
    }

    // @desc Get character
    // @route GET /api/v1/characters/
    // @access Private
    getCharacter(req, res) {
        try {
            fetch('https://rickandmortyapi.com/api/character/')
                .then(resp => resp.json())
                .then(body => {
                    const characters = body.results.map(character => {
                        return {
                            name: character.name,
                            status: character.status,
                            species: character.species,
                            gender: character.gender,
                            img: character.image
                        }
                    })
                    res.status(200).send(characters)
                })
        } catch (e) {
            console.error(e);
            res.status(500);
        }
    }
}

