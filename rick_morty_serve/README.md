
# rick_morty_server
Web to show Rick & Morty characters


## Requirements
- NodeJS 12.18.2

## Instructions

### Enviroment setup
1. Do npm install
2. Setup .env config file on path config/config.env
```

PASSWORD_SALT

TOKEN_HASH
TOKEN_EXPIRATION_SECONDS
```
Note: this project use https://rickandmortyapi.com/api/character/ as a provider to obtain the characters of the series

3. Run node server
