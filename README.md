# ric_morty_app

In this container there is an app to be able to visualize the characters of rick and morty safely

## Getting Started

1.- Clone this repository
```shell
git clone https://gitlab.com/ronalfonso/rick_morty_app
```

2.- Build the image
```shell
docker-compose Build
```

3.- Up the image
```shell
docker-compose up
```

4.- Open your browser and enter the address
```shell
http://localhost:8080
```

## Built With

* Frontend with react v17.0.1
* Backend with nodejs v12.18.3
* Data base with redis v6.0.9


## Authors

* **Ronald Alfonso** 