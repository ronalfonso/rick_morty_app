
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.scss$/,
                use: ['style-loader', 'sass-loader'],
            },
        ]

    },
    output: {
        filename: "js/main.[hash].js",
        publicPath: "/"
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "WebDevServer",
            filename: './index.html',
            template: './src/index.html'
        })

    ]
}
