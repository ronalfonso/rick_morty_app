import React from 'react';
import ReactDOM from 'react-dom';


import 'bootswatch/dist/minty/bootstrap.min.css';
import RickAndMortyApp from "./RickAndMortyApp";

const App = () => {
    return (
        <RickAndMortyApp/>
    )
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
)
