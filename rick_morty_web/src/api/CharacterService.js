import React from 'react';
import BaseService from "./BaseService";
import axios from 'axios';

export default class CharacterService extends React.Component {

    static getClassUrl() {
        return 'characters'
    }

    static getUrl() {
        return `${BaseService.getBaseUrl()}${CharacterService.getClassUrl()}`;
    }

    static getListCharacters(username, token){
        let headers = {
            token: token
        }
        return axios.get(`${CharacterService.getUrl()}/${username}`,  {headers}).then( res => {

            return res.data;
        }, (error) => {
            console.error({error});
            return error
        }).catch(err => {
            if (err.response) {
                return err
            }
        })
    }
}
