import React from 'react';
import './styles.css';

const Card = ({character}) => {

    const {name, status, img, species, gender} = character;
    return (
        <div className="card">
            <img src={img} alt={name}/>
            <div className="info-character">
                <h6><strong>{name}</strong></h6>
                <div className="data">
                    <span>Status</span>
                    <p>{status}</p>
                </div>
                <div className="data">
                    <span>Specie</span>
                    <p>{species}</p>
                </div>
                <div className="data">
                    <span>Gender</span>
                    <p>{gender}</p>
                </div>


            </div>
        </div>
    )
}

export default Card;
